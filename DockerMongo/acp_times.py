"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#
km_list = [200, 400, 600, 1000, 1300]
min_spd = [15, 15, 15, 11.428, 13.333]
max_spd = [34, 32, 30, 28, 16]


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    _open = arrow.get(brevet_start_time)
    hour = 0
    temp_control_km = control_dist_km
    if control_dist_km > brevet_dist_km:
      temp_control_km = brevet_dist_km
    remain_control_km = temp_control_km
    for i in range(5):
      if temp_control_km <= km_list[i]:
        if i == 0:
          between_station_km = min(remain_control_km, km_list[0])
        else:
          between_station_km = min(remain_control_km, (km_list[i]-km_list[i-1]))
        hour = hour + round(between_station_km/max_spd[i], 2)
        remain_control_km -= between_station_km
      else:
        continue
    print(hour)
    if temp_control_km < 0:
      hour = 0
    return _open.shift(hours = hour).isoformat()
    #return arrow.now().isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    _close = arrow.get(brevet_start_time)
    hour = 0
    temp_control_km = control_dist_km
    if control_dist_km > brevet_dist_km:
      temp_control_km = brevet_dist_km
    remain_control_km = temp_control_km
    for i in range(5):
      if temp_control_km <= km_list[i]:
        if i == 0:
          between_station_km = min(remain_control_km, km_list[0])
        else:
          between_station_km = min(remain_control_km, (km_list[i]-km_list[i-1]))
        print(remain_control_km) 
        hour += round(between_station_km/min_spd[i], 2)
        remain_control_km -= between_station_km
      else:
        continue
    print(hour)
    if temp_control_km < 0:
      hour = 0

    return _close.shift(hours = hour).isoformat()
    #return arrow.now().isoformat()
def test_open():
  assert open_time(50, 200, '2018-01-01T00:00:00-07:00') == '2018-01-01T01:28:12-07:00'
  assert open_time(205, 200, '2018-01-01T00:00:00-07:00') == '2018-01-01T05:52:48-07:00'
  assert close_time(50, 200, '2018-01-01T00:00:00-07:00') == '2018-01-01T03:19:48-07:00'
